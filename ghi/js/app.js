function createCard(name, description, pictureUrl, startDate, endDate, location) {
    return `
    <div class="col-4 gy-3">
      <div class="card shadow-lg">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
          <p class="card-text">${description}</p>
          <div class="card-footer">${startDate} - ${endDate}</div>
        </div>
      </div>
    </div>
    `;
  }

function throwError(e) {
    return `
    <div class="alert alert-danger" role="alert">${e}</div>
    `;
}


  window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    try {
        const response = await fetch(url);



      if (!response.ok) {
        throw new Error('Response not ok');
      } else {
        const data = await response.json();

        for (let conference of data.conferences) {
            const detailUrl = `http://localhost:8000${conference.href}`;
            const detailResponse = await fetch(detailUrl);
            if (detailResponse.ok) {
              const details = await detailResponse.json();
              console.log(details);
              const title = details.conference.name;  // changed to name
              const description = details.conference.description;
              const pictureUrl = details.conference.location.picture_url;
              const startDate = (new Date(details.conference.starts)).toLocaleDateString();
              const endDate = (new Date(details.conference.ends)).toLocaleDateString();
              const location = details.conference.location.name
              const html = createCard(title, description, pictureUrl, startDate, endDate, location);
              const column = document.querySelector('.row');
              column.innerHTML += html;
            }
      }
    }
    } catch (e) {
        console.error(e);
        const errorMessage = throwError(e)
        const message = document.querySelector('#card_error');
        message.innerHTML += errorMessage;
      }
  });


  // fetch is going to url location to pull data and returns a promise
