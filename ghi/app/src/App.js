import { BrowserRouter, Routes, Route } from "react-router-dom";
import Nav from './Nav';
import AttendeesList from './AttendeesList';
import LocationForm from './LocationForm';
import ConferenceForm from './ConferenceForm';
import AttendConferenceForm from './attend-conference';

function App(props) {
  if (props.attendees === undefined) {
    return null;
  }
  return (
<BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="attendees">
            <Route path="" element={<AttendeesList attendees={props.attendees}/>}/>
            <Route path="new" element={<AttendConferenceForm />} />
          </Route>
          <Route path="locations">
            <Route path="new" element={<LocationForm />} />
          </Route>
          <Route path="conferences">
            <Route path="new" element={<ConferenceForm />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
    );
}

export default App;
